
/*#include "global_constants.h"
//#include <iomanip>
#include <iostream>
#include <cmath>
//#include <array>

using namespace std;*/

/****************************************************************************
Function Name: gauss_dist
Programmer: Tessa Herzberger
Parameters taken: Double array, double array, double array
Parameter returned: Double
Program Description: Calls the calc_average and cald_std_deviation
functions to determine the gaussian
distribution of the RSSI sample array. Then calls the
prob_dens_func_dataPoint function to
determine the probability of receiving each
coresponding sample value. Next, calls the
gaussian_prediction function to predict the
most likely value from the sample array.
After that, the most likely value is used to
filter out values that are more than two
stndard deviations away from the most likely
value. The gaussian_prediction function is
called again using the filtered (new) sample
data set, and he result is returned.
****************************************************************************/ 
/*double alpha_trimmed_mean(double probability_array[], double samples[], double new_samples[], double window_size)
{
	double temp_return = 182;
	double window_size_avg = 0;
	//double alpha_array[MAX_SAMPLES];
	//samples = A
	//new_samples = AA
	//window_size_avg = ww

	if ((window_size % 2) == 1)
	{
		if ((alpha > 0) && (alpha < 0.5))
		{
			if (window_size <= MAX_SAMPLES)
			{
				window_size_avg = (window_size - 1) / 2;

				for (int i = 0; i < MAX_SAMPLES; i++)
				{
					if (samples[i != DEAD_BEEF_VAL])
					{
						//FIX VV
						s = max(1, i - ww);
						e = min(i + ww, length(A));
						window = A(s:e);
						window = sort(window);
						t = floor(determine_array_length(samples) * alpha);
						window = window(t + 1:end - t);
						//FIX ^^

						

						new_samples[i] = ATM_calc_average(window);
					}
				}

			}
			else
				cout << "Window size is out of range" << endl;
		}
		else
			cout << "Alpha is is out of range" << endl;
	}
	else
		cout << "Window size must be odd" << endl;


	return temp_return;
}*/


/****************************************************************************
Function Name: ATM_calc_average
Programmer: Tessa Herzberger
Parameters taken: double array
Parameter returned: double
Program Description: Calculates the average for a given array
****************************************************************************/
/*double ATM_calc_average(double samples[])
{
	double sum = 0;
	double counter = 0;

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (samples[i] != DEAD_BEEF_VAL)
		{
			sum += samples[i];
			counter++;
		}
	}

	//	cout << "AVERAGE = " << (sum / counter) << endl;
	return (sum / counter);
}*/

/*double determine_array_length(double samples[])
{
	double counter = 0;

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (samples[i] != DEAD_BEEF_VAL)
			counter++;
	}

	return counter;
}*/

/*double ATM_find_max_array_value(double samples[])
{
	double max_value = 0;

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (samples[i] != DEAD_BEEF_VAL)
		{
			if (samples[i] > max_value)
				max_value = samples[i];
		}
	}

	return max_value;
}*/

/*double ATM_find_min_array_value(double samples[])
{
	double min_value = 0;

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (samples[i] != DEAD_BEEF_VAL)
		{
			if (samples[i] < min_value)
				min_value = samples[i];
		}
	}

	return min_value;
}*/