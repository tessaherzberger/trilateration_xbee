/****************************************************************************
****************************************************************************
		File Name: Trilateration_Filtering.cpp
		Programmer: Tessa Herzberger
		Functions within the File:
					- trilateration_filtered_rssi
		Created: January 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/
#include "global_constants.h"
#include "GaussianAndStatisticalAverage.h"
#include "gaussian_dist.h"
#include "statistical_avg.h"
#include "Trilateration_Filtering.h"


/****************************************************************************
		Function Name: trilateration_filtered_rssi
		Programmer: Tessa Herzberger
		Parameters taken: double array, double
		Parameter returned: double
		Program Description: Determines what type of filter will be used
							 to determine the most likely RSSI value.
							 The appropriate filter function is called
							 depending on user input (the filter type).
							 After the most likely RSSI value has been
							 determined by its respective function,
							 this value is returned.
****************************************************************************/
double trilateration_filtered_rssi(double samples[], double filter_type)
{
	double probability_array[MAX_SAMPLES] = {};
	double new_samples[MAX_SAMPLES] = {};
	double rssi_value = 0; 

	if (filter_type == 1)
		rssi_value = gauss_dist_and_statistical_avg_filter(probability_array, samples, new_samples);
	else if (filter_type == 2)
		rssi_value = gauss_dist(probability_array, samples, new_samples);
	else if (filter_type == 3)
		rssi_value = calc_stat_avg(samples);
	
	return rssi_value;
}