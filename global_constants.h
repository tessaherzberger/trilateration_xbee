/****************************************************************************
****************************************************************************
		File Name: global_constants.h
		Programmer: Tessa Herzberger
		Created: January 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/

#ifndef GLOBAL_CONSTANTS_H
#define GLOBAL_CONSTANTS_H


#define NUM_DIRECTIONS 3
#define PI_NUM 3.1415926535897932384
#define MIN_PROBABILITY 0.01
#define DEAD_BEEF_VAL 9999
#define MAX_SAMPLES 20
#define MAX_METERS 20
#define M 4
#define N 4
#define n_constant_trilateration 4
#define n_constant_basement_baseline 2.325
#define n_constant_2_4_GHz 1.467
#define n_constant_underground 2.02
#define A_constant_trilateration -29
#define A_constant_basement_baseline -32
#define A_constant_2_4_GHz -34
#define A_constant_underground -34

const double ZERO_METERS_BASELINE[20] = { -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double ONE_METER_BASELINE[20] = { -33, -34, -33, -32, -34, -31, -31, -31, -32, -33, -33, -33, -31, -32, -31, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TWO_METERS_BASELINE[20] = { -37, -40, -38, -39, -40, -39, -38, -40, -41, -37, -40, -40, -39, -40, -40, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double THREE_METERS_BASELINE[20] = { -42, -42, -40, -41, -44, -41, -42, -43, -41, -41, -42, -41, -44, -44, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double FOUR_METERS_BASELINE[20] = { -46, -44, -46, -45, -45, -45, -45, -44, -46, -46, -45, -47, -47, -45, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double FIVE_METERS_BASELINE[20] = { -45, -44, -41, -40, -42, -41, -41, -43, -41, -42, -42, -43, -43, -40, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double SIX_METERS_BASELINE[20] = { -59, -50, -46, -51, -48, -47, -49, -48, -48, -52, -52, -52, -50, -46, -51, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL};
const double SEVEN_METERS_BASELINE[20] = { -54, -60, -65, -58, -54, -55, -53, -55, -55, -62, -56, -56, -62, -62, -54, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double EIGHT_METERS_BASELINE[20] = { -44, -44, -44, -43, -43, -44, -44, -44, -43, -43, -43, -44, -44, -44, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double NINE_METERS_BASELINE[20] = { -57, -47, -50, -50, -48, -49, -50, -48, -48, -50, -48, -49, -49, -47, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TEN_METERS_BASELINE[20] = { -48, -47, -51, -50, -53, -54, -55, -54, -53, -55, -55, -56, -53, -54, -54, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double FIFTEEN_METERS_BASELINE[20] = { -49, -45, -48, -48, -47, -51, -50, -46, -46, -45, -50, -47, -49, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TWENTY_METERS_BASELINE[20] = { -52, -52, -52, -54, -54, -54, -55, -55, -54, -53, -49, -54, -53, -51, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TWENTY_FIVE_METERS_BASELINE[20] = { -49, -47, -47, -47, -46, -46, -45, -45, -45, -45, -45, -45, -47, -46, -46, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double THIRTY_METERS_BASELINE[20] = { -52, -53, -51, -54, -53, -53, -53, -55, -52, -53, -52, -54, -55, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };

const double TRILATERATION_1_METER[20] = {-31, -28, -28, -29, -29, -28, -28, -28, -28, -28, -28, -29, -29, -29, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TRILATERATION_1_8_METERS[20] = { -37, -38, -37, -40, -39, -39, -39, -40, -37, -37, -37, -38, -37, -38, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TRILATERATION_2_METERS[20] = { -45, -51, -39, -43, -42, -39, -37, -40, -40, -40, -41, -40, -40, -40, -42, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TRILATERATION_2_5_METERS[20] = { -45, -40, -42, -41, -41, -42, -40, -42, -41, -42, -42, -41, -40, -42, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TRILATERATION_3_METERS[20] = { -44, -40, -44, -42, -40, -43, -43, -42, -41, -40, -42, -41, -45, -43, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TRILATERATION_4_METERS[20] = { -54, -44, -42, -42, -45, -44, -44, -44, -45, -45, -47, -45, -47, -50, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double TRILATERATION_4_47_METERS[20] = { -46,6-45, -41, -41, -42, -42, -41, -40, -41, -42, -45, -43, -43, -42, -47, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double TRILATERATION_5_7_METERS[20] = { -44, -40, -41, -42, -42, -40, -41, -41, -43, -42, -41, -44, -42, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TRILATERATION_8_02_METERS[20] = { -46, -47, -47, -47, -47, -47, -47, -45, -46, -45, -47, -45, -45, -44, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };


const double ZERO_METER_2_4GHZ[20] = {};
const double ONE_METER_2_4GHZ[20] = { -33, -34, -35, -34, -34, -34, -34, -34, -34, -34, -34, -34, -35, -34, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double ONE_EIGHT_METER_2_4GHZ[20] = { -35, -34, -35, -36, -35, -35, -36, -35, -35, -34, -34, -35, -37, -34, -34, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TWO_METER_2_4GHZ[20] = { -35, -37, -38, -36, -36, -37, -37, -36, -36, -35, -36, -36, -36, -37, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double TWO_FIVE_METER_2_4GHZ[20] = { -46, -42, -45, -45, -45, -46, -44, -43, -46, -43, -43, -43, -44, -43, -41, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double THREE_METER_2_4GHZ[20] = { -40, -41, -42, -41, -42, -41, -40, -40 , -40 , -40 , -40 , -40 , -40 , -40, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double FOUR_METER_2_4GHZ[20] = { -49, -51, -50, -50, -51, -46, -54, -54, -51, -62, -54, -53, -51, -53, -51, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double FIVE_METER_2_4GHZ[20] = {-45. -45, -44, -43, -43, -45, -45, -45, -45, -44, -45, -45, -44, -44, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double SIX_METER_2_4GHZ[20] = { -43, -38 ,-39, -40, -40, -40, -41, -41, -41, -42, -40, -42, -42, -41, -41, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double SEVEN_METER_2_4GHZ[20] = { -45, -52, -51, -52, -50, -55, -46, -48, -48, -50, -49, -54, -53, -49, -48, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double EIGHT_METER_2_4GHZ[20] = { -49, -47, -49, -45, -44, -45, -44, -49, -46, -45, -43, -45, -46, -45, -44, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double NINE_METER_2_4GHZ[20] = { -45, -46, -46, -46, -46, -47, -47, -47, -48, -49, -46, -45, -47, -47, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double TEN_METER_2_4GHZ[20] = { -56, -54, -51, -50, -51, -50, -51, -50, -50, -50, -50, -47, -50, -51, -50, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };








const double UNFILTERED_DATA_UNDERGROUND[20] = { -8, -32, -40, -44, -46, -50, -53, -50, -54, -57, -51, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL};
const double ZERO_METERS_UNDERGROUND[20] = { -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, -8, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double ONE_METER_UNDERGROUND[20] = { -32, -32, -33, -34, -34, -34, -33, -34, -34, -34, -35, -34, -34, -35, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TWO_METERS_UNDERGROUND[20] = { -41, -40, -40, -40, -40, -40, -39, -39, -40, -41, -40, -40, -41, -41, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double THREE_METERS_UNDERGROUND[20] = { -46, -44, -45, -46, -45 -46, -45, -45, -46, -46, -47, -49, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double FOUR_METERS_UNDERGROUND[20] = { -46, -46, -45, -46, -47, -47, -46, -45, -46, -46, -48, -46, -46, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double FIVE_METERS_UNDERGROUND[20] = { -50, -50, -52, -52, -53, -51, -52, -53, -54, -50, -51, -50, -50, -50, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double SIX_METERS_UNDERGROUND[20] = { -56, -53, -51, -53, -51, -53, -53, -53, -54, -55, -55, -55, -54, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double SEVEN_METERS_UNDERGROUND[20] = { -50, -50, -51, -52, -52, -51, -49, -51, -51, -52, -52, -48, -50, -50, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double EIGHT_METERS_UNDERGROUND[20] = { -53, -54, -53, -55, -53, -51, -52, -50, -52, -54, -52, -54, -53, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
//const double NINE_METERS_UNDERGROUND[20] = { -57, -57, -62, -57, -56, -61, -56, -58, -58, -55, -60, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double NINE_METERS_UNDERGROUND[20] = { -57, -57, -64, -57, -62, -57, -56, -61, -56, -58, -58, -55, -60, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };


const double TEN_METERS_UNDERGROUND[20] = { -52, -51, -53, -52, -52, -52, -52, -52, -52, -52, -52, -52, -50, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double FIFTEEN_METERS_UNDERGROUND[20] = { -59, -58, -59, -58, -60, -58, -58, -57, -57, -60, -59, -59, -59, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TWENTY_METERS_UNDERGROUND[20] = { -70, -65, -68, -72, -75, -71, -67, -77, -76, -71, -67, -65, -71, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double TWENTY_FIVE_METERS_UNDERGROUND[20] = { -59, -59, -57, -60, -58, -57, -58, -57, -58, -58, -58, -58, -58, -58, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double THIRTY_METERS_UNDERGROUND[20] = { -67, -69, -66, -72, -69, -70, -70, -70, -72, -72, -73, -73, -70, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
const double FOURTY_METERS_UNDERGROUND[20] = { -58, -58, -58, -56, -57, -57, -57, -56, -57, -56, -57, -56, -57, -58, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double FIFTY_METERS_UNDERGROUND[20] = { -64, -64- -65, -66, -65, -65, -64, -62, -64, -64, -63, -64, -65, -66, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
const double SIXTY_METERS_UNDERGROUND[20] = { -61, -61, -60, -60, -60, -60, -60, -61, -61, -6, -62, -62, -61, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };












#endif GLOBAL_CONSTANTS_H
