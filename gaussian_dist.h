/****************************************************************************
****************************************************************************
		File Name: gaussian_dist.h
		Programmer: Tessa Herzberger
		Created: January 2020
		Last Modified: March 2020
****************************************************************************
****************************************************************************/

#ifndef GAUSSIAN_DIST_H
#define GAUSSIAN_DIST_H


double gauss_dist(double probability_array[], double samples[], double new_samples[]);
double calc_average(double samples[]);
double calc_std_deviation(double average, double samples[]);
double prob_dens_func_dataPoint(double sample, double average, double std_dev);
//void gaussian_filter(double samples[], double prob_array[], double new_samples[]);
void perform_gaussian_filter(double samples[], double probability_array[], double new_samples[], double predicted_vals[]);
double gaussian_prediction(double probability_array[], double samples[]);
void perform_gaussian_filter_underground(double samples[], double probability_array[], double new_samples[], double predicted_vals[]);

#endif GAUSSIAN_DIST_H
