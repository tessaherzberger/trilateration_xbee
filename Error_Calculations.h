/****************************************************************************
****************************************************************************
		File Name: Error_Calculations.h
		Programmer: Tessa Herzberger
		Created: March 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/

#ifndef ERROR_CALCULATIONS_H
#define ERROR_CALCULATIONS_H


void absolute_error_calculations(double theoretical_values[], double measured_values[], double absolute_errors[]);
void absolute_error_calculations_x_y_z(double actual_values[], double measured_values[], double absolute_errors[]);
double radius_error_calculations(double absolute_errors[]);


#endif ERROR_CALCULATIONS_H