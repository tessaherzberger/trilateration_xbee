/****************************************************************************
****************************************************************************
		File Name: gaussian_dist.cpp
		Programmer: Tessa Herzberger
		Functions within the File:
					- gauss_dist
					- calc_average
					- calc_std_deviation
					- prob_dens_func_dataPoint
					- perform_gaussian_filter
					- gaussian_prediction
		Created: January 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/


#include "gaussian_dist.h"
#include "global_constants.h"
#include "math.h"
#include <iostream>

using namespace std;

/****************************************************************************
		Function Name: gauss_dist
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double array, double array
		Parameter returned: Double
		Program Description: Calls the calc_average and cald_std_deviation
							 functions to determine the gaussian
							 distribution of the RSSI sample array. Then calls the
							 prob_dens_func_dataPoint function to
							 determine the probability of receiving each 
							 coresponding sample value. Next, calls the
							 gaussian_prediction function to predict the
							 most likely value from the sample array.
							 After that, the most likely value is used to
							 filter out values that are more than two
							 stndard deviations away from the most likely 
							 value. The gaussian_prediction function is
							 called again using the filtered (new) sample
							 data set, and he result is returned.
****************************************************************************/
double gauss_dist(double probability_array[], double samples[], double new_samples[])
{
	double probability_value = 0;
	double average = 0.0;
	double std_dev = 0.0;
	double most_likely_sample = 0.0;
	double predicted_value = 0.0;

	average = calc_average(samples);
	std_dev = calc_std_deviation(average, samples);

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		probability_array[i] = prob_dens_func_dataPoint(samples[i], average, std_dev);
		if (isnan(probability_array[i]))
			probability_array[i] = 100;
	}

	most_likely_sample = gaussian_prediction(probability_array, samples);

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (((samples[i] > (most_likely_sample + (std_dev * 2.0))) || (samples[i] < (most_likely_sample - (std_dev * 2.0)))) && (probability_array[i] != 100) && (samples[i] != DEAD_BEEF_VAL))
			new_samples[i] = DEAD_BEEF_VAL;
		else
			new_samples[i] = samples[i];
	}

	predicted_value = gaussian_prediction(probability_array, new_samples);

	return predicted_value;
}

/****************************************************************************
		Function Name: calc_average
		Programmer: Tessa Herzberger
		Parameters taken: double array
		Parameter returned: double
		Program Description: Calculates the average for a given array
****************************************************************************/
double calc_average(double samples[])
{
	double sum = 0;
	double counter = 0;

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (samples[i] != DEAD_BEEF_VAL)
		{
			sum += samples[i];
			counter++;
		}
	}

	return (sum/counter);
}


/****************************************************************************
		Function Name: calc_std_deviation
		Programmer: Tessa Herzberger
		Parameters taken: double, double array
		Parameter returned: double
		Program Description: Calculates the standard deviation for a 
							 given array and average value.
****************************************************************************/
double calc_std_deviation(double average, double samples[])
{
	double variance = 0;
	double counter = 0;

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (samples[i] != DEAD_BEEF_VAL)
		{
			variance += pow(samples[i] - average, 2);
			counter++;
		}
	}
	variance /= (counter - 1);

	return sqrt(variance);
}

/****************************************************************************
		Function Name: prob_dens_func_dataPoint
		Programmer: Tessa Herzberger
		Parameters taken: double, double, double
		Parameter returned: double
		Program Description: Determines the probability using the
							 probability density function for a given 
							 sample value within an array of sample values
****************************************************************************/
double prob_dens_func_dataPoint(double sample, double average, double std_dev)
{
	double prob = 0;
	double prob_0 = 0;
	double prob_1 = 0;
	double exp_0 = 0;
	double exp_1 = 0;
	double exp_2 = 0;

	exp_0 = pow((sample - average), 2);
	exp_0 = -1 * exp_0;
	exp_1 = pow(std_dev, 2);
	exp_1 = 2 * exp_1;
	exp_2 = exp_0 / exp_1;

	prob_0 = exp(exp_2);
	prob_1 = std_dev * sqrt(2 * PI_NUM);
	prob = prob_0 / prob_1;

	return prob;
}

/****************************************************************************
		Function Name: perform_gaussian_filter
		Programmer: Tessa Herzberger
		Parameters taken: double array, double array, double array,
						  double array
		Parameter returned: Nothing
		Program Description: Performs the Gaussian Filter for a set of
							 data points at various distances. This is
							 done by defining the samples array
							 (which contains the set of data points),
							 then calling the gauss_dist function for
							 each set of data points.
****************************************************************************/
void perform_gaussian_filter(double samples[], double probability_array[], double new_samples[], double predicted_vals[])
{
	for (int i = 0; i < MAX_METERS; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				//samples[j] = ZERO_METER_2_4GHZ[j];
				samples[j] = ZERO_METERS_BASELINE[j];
		}
		if (i == 1)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ONE_METER_2_4GHZ[j];
				//samples[j] = ONE_METER_BASELINE[j];
		}
		if (i == 2)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWO_METER_2_4GHZ[j];
				//samples[j] = TWO_METERS_BASELINE[j];
		}
		if (i == 3)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THREE_METER_2_4GHZ[j];
				//samples[j] = THREE_METERS_BASELINE[j];
		}
		if (i == 4)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FOUR_METER_2_4GHZ[j];
				//samples[j] = FOUR_METERS_BASELINE[j];
		}
		if (i == 5)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIVE_METER_2_4GHZ[j];
				//samples[j] = FIVE_METERS_BASELINE[j];
		}
		if (i == 6)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SIX_METER_2_4GHZ[j];
				//samples[j] = SIX_METERS_BASELINE[j];
		}
		if (i == 7)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SEVEN_METER_2_4GHZ[j];
				//samples[j] = SEVEN_METERS_BASELINE[j];
		}
		if (i == 8)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = EIGHT_METER_2_4GHZ[j];
				//samples[j] = EIGHT_METERS_BASELINE[j];
		}
		if (i == 9)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = NINE_METER_2_4GHZ[j];
				//samples[j] = NINE_METERS_BASELINE[j];
		}
		if (i == 10)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TEN_METER_2_4GHZ[j];
				//samples[j] = TEN_METERS_BASELINE[j];
		}
		if (i == 15)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIFTEEN_METERS_BASELINE[j];
		}
		if (i == 20)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_METERS_BASELINE[j];
		}
		if (i == 25)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_FIVE_METERS_BASELINE[j];
		}
		if (i == 30)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THIRTY_METERS_BASELINE[j];
		}

		if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 ||
			i == 9 || i == 10 || i == 15 || i == 20 || i == 25 || i == 30)
		{
			predicted_vals[i] = gauss_dist(probability_array, samples, new_samples);
			cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
		}
		else
			predicted_vals[i] = DEAD_BEEF_VAL;
	}

	return;
}

/****************************************************************************
		Function Name: gaussian_prediction
		Programmer: Tessa Herzberger
		Parameters taken: double array, double array
		Parameter returned: Nothing
		Program Description: Determines the predicted value using an array
							 of probability values which correspond to an
							 array of sample values. The sample value with
							 the highest corresponding probability value
							 is returned as the predicted value.
****************************************************************************/
double gaussian_prediction(double probability_array[], double samples[])
{
	double prediction = 0.0;
	double current_max = probability_array[0];
	int max_location = 0;

	for (int i = 1; i < MAX_SAMPLES; i++)
	{
		if (current_max < probability_array[i])
		{
			current_max = probability_array[i];
			max_location = i;
		}
	}

	return samples[max_location];
}





/****************************************************************************
Function Name: perform_gaussian_filter
Programmer: Tessa Herzberger
Parameters taken: double array, double array, double array,
double array
Parameter returned: Nothing
Program Description: Performs the Gaussian Filter for a set of
data points at various distances. This is
done by defining the samples array
(which contains the set of data points),
then calling the gauss_dist function for
each set of data points.
****************************************************************************/
void perform_gaussian_filter_underground(double samples[], double probability_array[], double new_samples[], double predicted_vals[])
{
	for (int i = 0; i < MAX_METERS; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				//samples[j] = ZERO_METER_2_4GHZ[j];
				samples[j] = ZERO_METERS_UNDERGROUND[j];
		}
		if (i == 1)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ONE_METER_UNDERGROUND[j];
			//samples[j] = ONE_METER_BASELINE[j];
		}
		if (i == 2)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWO_METERS_UNDERGROUND[j];
			//samples[j] = TWO_METERS_BASELINE[j];
		}
		if (i == 3)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THREE_METERS_UNDERGROUND[j];
			//samples[j] = THREE_METERS_BASELINE[j];
		}
		if (i == 4)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FOUR_METERS_UNDERGROUND[j];
			//samples[j] = FOUR_METERS_BASELINE[j];
		}
		if (i == 5)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIVE_METERS_UNDERGROUND[j];
			//samples[j] = FIVE_METERS_BASELINE[j];
		}
		if (i == 6)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SIX_METERS_UNDERGROUND[j];
			//samples[j] = SIX_METERS_BASELINE[j];
		}
		if (i == 7)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SEVEN_METERS_UNDERGROUND[j];
			//samples[j] = SEVEN_METERS_BASELINE[j];
		}
		if (i == 8)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = EIGHT_METERS_UNDERGROUND[j];
			//samples[j] = EIGHT_METERS_BASELINE[j];
		}
		if (i == 9)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = NINE_METERS_UNDERGROUND[j];
			//samples[j] = NINE_METERS_BASELINE[j];
		}
		if (i == 10)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TEN_METERS_UNDERGROUND[j];
			//samples[j] = TEN_METERS_BASELINE[j];
		}
		if (i == 15)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIFTEEN_METERS_UNDERGROUND[j];
		}
		if (i == 20)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_METERS_UNDERGROUND[j];
		}
		if (i == 25)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_FIVE_METERS_UNDERGROUND[j];
		}
		if (i == 30)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THIRTY_METERS_UNDERGROUND[j];
		}

		if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 ||
			i == 9 || i == 10 || i == 15 || i == 20 || i == 25 || i == 30)
		{
			predicted_vals[i] = gauss_dist(probability_array, samples, new_samples);
			cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
		}
		else
			predicted_vals[i] = DEAD_BEEF_VAL;
	}

	return;
}