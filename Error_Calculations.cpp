/****************************************************************************
****************************************************************************
		File Name: Error_Calculations.cpp
		Programmer: Tessa Herzberger
		Functions within the File:
					- absolute_error_calculations_x_y_z
					- radius_error_calculations
		Created: March 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/


#include "Error_Calculations.h"
#include "global_constants.h"
#include <iostream>
#include <cmath>

using namespace std;


/****************************************************************************
Function Name: absolute_error_calculations
Programmer: Tessa Herzberger
Parameters taken: Double array, double array, double array
Parameter returned: Nothing
Program Description: Calculates the absolute error of the theoretical
x, y and z coordinate values and the
measured values.
****************************************************************************/
void absolute_error_calculations(double theoretical_values[], double measured_values[], double absolute_errors[])
{
//	for (int i = 0; i < MAX_SAMPLES; i++)
//	{
//		cout << "THEORETICAL = " << theoretical_values[i] << endl;
///		cout << "MEASURED = " << measured_values[i] << endl;
//	}

	for (int i = 0; i < MAX_SAMPLES; i++)
		absolute_errors[i] = abs(theoretical_values[i] - measured_values[i]);
	//	absolute_errors[0] = abs(theoretical_values[0] - measured_values[0]);

	return;
}

/****************************************************************************
		Function Name: absolute_error_calculations_x_y_z
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double array, double array
		Parameter returned: Nothing
		Program Description: Calculates the absolute error of the theoretical
							 x, y and z coordinate values and the
							 measured values.
****************************************************************************/
void absolute_error_calculations_x_y_z(double theoretical_values[], double measured_values[], double absolute_errors[])
{
	for (int i = 0; i < (M-1); i++)
		absolute_errors[i] = abs(theoretical_values[i] - measured_values[i]);
//	absolute_errors[0] = abs(theoretical_values[0] - measured_values[0]);

	return;
}

/****************************************************************************
		Function Name: radius_error_calculations
		Programmer: Tessa Herzberger
		Parameters taken: Double array
		Parameter returned: Double
		Program Description: Determines the radius error of the
							 pre-calculated absolute errors
							 (calculated by the function
							 absolute_error_calculations_x_y_z).
****************************************************************************/
double radius_error_calculations(double absolute_errors[])
{
	double radius_error = 0;
	double squared_error[(M - 1)];
	for (int i = 0; i < (M-1); i++)
		squared_error[i] = pow(absolute_errors[i], 2);
	cout << "Squared Error = " << squared_error[0] << ", " << squared_error[1] << ", " << squared_error[2] << endl;


	for (int i = 0; i < (M - 1); i++)
		radius_error += squared_error[i];

	cout << "Radius Error = " << radius_error << endl;

	radius_error = sqrt(radius_error);

	cout << "Radius Error = " << radius_error << endl;

	return radius_error;
}