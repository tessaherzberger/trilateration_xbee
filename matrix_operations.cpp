/****************************************************************************
****************************************************************************
		File Name: matrix_operations.cpp
		Programmer: Tessa Herzberger
		Functions within the File:
					- inverse_matrix
					- construct_trilateration_vector
					- multiple_matrix_by_vector
		Created: January 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/
#include "global_constants.h"
#include "matrix_operations.h"
#include "math.h"

/****************************************************************************
		Function Name: inverse_matrix
		Programmer: Tessa Herzberger
		Parameters taken: Double two-dimensional array, double array,
						  double array, double array, double array
		Parameter returned: Nothing
		Program Description: Calculates the inverse of a matrix
							 constructed from four arrays.
****************************************************************************/
void inverse_matrix(double matrix[][3], double node1[], double node2[], double node3[], double node4[])
{
	double mat_11, mat_12, mat_13;
	double mat_21, mat_22, mat_23;
	double mat_31, mat_32, mat_33;
	double det;
	const int X = 0;
	const int Y = 1;
	const int Z = 2;

	//First Row of Inverse Matrix
	mat_11 = (node3[Y]*node2[Z]) - (node4[Y]*node2[Z]) - (node2[Y]*node3[Z]) + (node4[Y]*node3[Z]) + (node2[Y]*node4[Z]) - (node3[Y]*node4[Z]);
	mat_12 = (-1*node3[Y]*node1[Z]) + (node4[Y]*node1[Z]) + (node1[Y]*node3[Z]) - (node4[Y]*node3[Z]) - (node1[Y]*node4[Z]) + (node3[Y]*node4[Z]);
	mat_13 = (node2[Y]*node1[Z]) - (node4[Y]*node1[Z]) - (node1[Y]*node2[Z]) + (node4[Y]*node2[Z]) + (node1[Y]*node4[Z]) - (node2[Y]*node4[Z]);

	//Second Row of Inverse Matrix
	mat_21 = -1*node3[X]*node2[Z] + node4[X]*node2[Z] + node2[X]*node3[Z] - node4[X]*node3[Z] - node2[X]*node4[Z] + node3[X]*node4[Z];
	mat_22 = node3[X]*node1[Z] - node4[X]*node1[Z] - node1[X]*node3[Z] + node4[X]*node3[Z] + node1[X]*node4[Z] - node3[X]*node4[Z];
	mat_23 = -1*node2[X]*node1[Z] + node4[X]*node1[Z] + node1[X]*node2[Z] - node4[X]*node2[Z] - node1[X]*node4[Z] + node2[X]*node4[Z];

	//Third Row of Inverse Matrix
	mat_31 = node3[X]*node2[Y] - node4[X]*node2[Y] - node2[X]*node3[Y] + node4[X]*node3[Y] + node2[X]*node4[Y] - node3[X]*node4[Y];
	mat_32 = -1*node3[X]*node1[Y] + node4[X]*node1[Y] + node1[X]*node3[Y] - node4[X]*node3[Y] - node1[X]*node4[Y] + node3[X]*node4[Y];
	mat_33 = node2[X]*node1[Y] - node4[X]*node1[Y] - node1[X]*node2[Y] + node4[X]*node2[Y] + node1[X]*node4[Y] - node2[X]*node4[Y];

	det = 1 / (node2[X]*node3[Y]*node1[Z] - node2[X]*node4[Y]*node1[Z] - node1[X]*node3[Y]*node2[Z] + node1[X]*node4[Y]*node2[Z] - node2[X]*node1[Y]*node3[Z] + node1[X]*node2[Y]*node3[Z] - node1[X]*node4[Y]*node3[Z] + node2[X]*node4[Y]*node3[Z] + node4[X]*(node3[Y]*(node2[Z] - node1[Z]) + node2[Y]*(node1[Z] - node3[Z]) + node1[Y]*(node3[Z] - node2[Z])) + node2[X]* node1[Y]*node4[Z] - node1[X]*node2[Y]*node4[Z] + node1[X]*node3[Y]*node4[Z] - node2[X]*node3[Y]*node4[Z] + node3[X]*(node4[Y]*(node1[Z] - node2[Z]) + node1[Y]*(node2[Z] - node4[Z]) + node2[Y]*(node4[Z] - node1[Z])));


	matrix[0][0] = det * mat_11;
	matrix[0][1] = det * mat_12;
	matrix[0][2] = det * mat_13;

	matrix[1][0] = det * mat_21;
	matrix[1][1] = det * mat_22;
	matrix[1][2] = det * mat_23;
	
	matrix[2][0] = det * mat_31;
	matrix[2][1] = det * mat_32;
	matrix[2][2] = det * mat_33;

	return;
}

/****************************************************************************
		Function Name: construct_trilateration_vector
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double array, double array,
						  double array, double array, double array
		Parameter returned: Nothing
		Program Description: Calculates the vector used in trilateration
							 calculations.
****************************************************************************/
void construct_trilateration_vector(double vector[3], double node1[], double node2[], double node3[], double node4[], double radius_rssi_values[])
{
	const int X = 0;
	const int Y = 1;
	const int Z = 2;
	const int NUM_ROWS = 3;
	
	for (int i = 0; i < 3; i++)
	{
		node1[i] = pow(node1[i], 2);
		node2[i] = pow(node2[i], 2);
		node3[i] = pow(node3[i], 2);
		node4[i] = pow(node4[i], 2);
	}

	for (int i = 0; i < 4; i++)
		radius_rssi_values[i] = pow(radius_rssi_values[i], 2);

	vector[0] = (radius_rssi_values[0] - radius_rssi_values[3]) - (node1[X] - node4[X]) - (node1[Y] - node4[Y]) - (node1[Z] - node4[Z]);
	vector[1] = (radius_rssi_values[1] - radius_rssi_values[3]) - (node2[X] - node4[X]) - (node2[Y] - node4[Y]) - (node2[Z] - node4[Z]);
	vector[2] = (radius_rssi_values[2] - radius_rssi_values[3]) - (node3[X] - node4[X]) - (node3[Y] - node4[Y]) - (node3[Z] - node4[Z]);

	return;
}

/****************************************************************************
		Function Name: multiply_matrix_by_vector
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double two-dimensional array,
						  double array
		Parameter returned: Nothing
		Program Description: Calculates the result of multiplying a vector
							 by a matrix. Used in trilateration to multiply
							 the inverse matrix (calculated by the function
							 inverse_matrix) by the vector used in
							 trilateration (calculated by the function
							 construct_trilateration_vector).
****************************************************************************/
void multiply_matrix_by_vector(double result[], double matrix[][3], double vector[])
{
	result[0] = 0.5 * ((matrix[0][0] * vector[0]) + (matrix[0][1] * vector[1]) + (matrix[0][2] * vector[2]));
	result[1] = 0.5 * ((matrix[1][0] * vector[0]) + (matrix[1][1] * vector[1]) + (matrix[1][2] * vector[2]));
	result[2] = 0.5 * ((matrix[2][0] * vector[0]) + (matrix[2][1] * vector[1]) + (matrix[2][2] * vector[2]));
	
	return;
}