/****************************************************************************
****************************************************************************
		File Name: kalman_filter.cpp
		Programmer: Tessa Herzberger
		Functions within the File:
					- kalman_filter
					- kalman_update_average
					- kalman_update_variance
					- kalman_predict_average
					- kalman_predict_variance
					- kalman_prob_dens_func_dataPoint
					- kalman_prediction
					- perform_kalman_filter
					- removing_values_via_kalman_filter
		Created: February 2020
		Last Modified: 15 February 2020
****************************************************************************
****************************************************************************/

#include "kalman_filter.h"
#include "global_constants.h"
#include <iostream>
#include <cmath>

using namespace std;

//Created using https://towardsdatascience.com/kalman-filters-a-step-by-step-implementation-guide-in-python-91e7e123b968
/****************************************************************************
		Function Name: kalman_filter
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double array, double array, double array 
		Parameter returned: Double
		Program Description: Performs Kalman Filtering algorithm. This
							 function calls the kalman_update_average,
							 kalman_update_variance, kalman_predict_average
							 and kalman_predict_variance to determine the
							 average and variance that will be used in
							 determining the most likely sample value.
							 After that, the kalman_prob_dens_func_dataPoint
							 is called in order to calculate the
							 probabilities of the sample values. Then the
							 kalman_prediction function is called to
							 determine the sample value with the highest
							 probability, which is the most likely sample
							 value.
****************************************************************************/
double kalman_filter(double samples[], double prediction[], double probability_array[], double new_samples[])
{
	//Initial parameters
	double average = 0.0;
	double variance = 10000;
	double measurement_variance = 5.0;
	double motion_variance = 0.0;
	double predicted_value = 0.0;
	double most_likely_sample = 0.0;
	double std_dev = 0.0;
	double motions[MAX_SAMPLES] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (samples[i] != DEAD_BEEF_VAL)
		{
			average = kalman_update_average(average, samples[i], variance, measurement_variance);
			variance = kalman_update_variance(variance, measurement_variance);
//			cout << "Update: [{" << average << "}, {" << variance << "}]" << endl;

			average = kalman_predict_average(average, motions[i]);
			variance = kalman_predict_variance(variance, motion_variance);
//			cout << "Predicted: [{" << average << "}, {" << variance << "}]" << endl;
		}
	}

//	cout << endl << "Final Predicted: [{" << average << "}, {" << variance << "}]" << endl;
//	cout << endl;
	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		probability_array[i] = kalman_prob_dens_func_dataPoint(samples[i], average, variance);
		if (isnan(probability_array[i]))
			probability_array[i] = 100;

	//	cout << "Probability of iteration " << i << " : " << probability_array[i] << endl;
	}

	std_dev = sqrt(variance);
	most_likely_sample = kalman_prediction(probability_array, samples);

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (((samples[i] >(most_likely_sample + (std_dev * 2.0))) || (samples[i] < (most_likely_sample - (std_dev * 2.0)))) && (probability_array[i] != 100) && (samples[i] != DEAD_BEEF_VAL))
		{
			new_samples[i] = DEAD_BEEF_VAL;
		}
		else
			new_samples[i] = samples[i];
	}

	predicted_value = kalman_prediction(probability_array, samples);

	return predicted_value;
}

/****************************************************************************
		Function Name: kalman_update_average
		Programmer: Tessa Herzberger
		Parameters taken: double, double, double, double
		Parameter returned: double
		Program Description: Calculates the updated average using two
							 previously calculated averages and two
							 previously calculated variances.
****************************************************************************/
double kalman_update_average(double avg1, double avg2, double var1, double var2)
{
	double numerator = (var2 * avg1) + (var1 * avg2);
	double denominator = var2 + var1;
	return (numerator / denominator);
}

/****************************************************************************
		Function Name: kalman_update_variance
		Programmer: Tessa Herzberger
		Parameters taken: double, double
		Parameter returned: double
		Program Description: Calculates the updated variance using two
							 previously variances.
****************************************************************************/
double kalman_update_variance(double var1, double var2)
{
	double denominator = (1 / var2) + (1 / var1);
	return (1.0/denominator);
}

/****************************************************************************
		Function Name: kalman_predict_average
		Programmer: Tessa Herzberger
		Parameters taken: double, double
		Parameter returned: double
		Program Description: Calculates the predicted average using two
							 previously calculated averages.
****************************************************************************/
double kalman_predict_average(double average, double new_average)
{
	return average + new_average;
}

/****************************************************************************
Function Name: kalman_predict_variance
Programmer: Tessa Herzberger
Parameters taken: double, double
Parameter returned: double
Program Description: Calculates the predicted variance using two previously
					 calculated variances.
****************************************************************************/
double kalman_predict_variance(double variance, double new_variance)
{
	return variance + new_variance;
}

/****************************************************************************
Function Name: kalman_prob_dens_func_dataPoint
Programmer: Tessa Herzberger
Parameters taken: double, double, double
Parameter returned: double
Program Description: Determines the probability using the
probability density function for a given
sample value within an array of sample values
****************************************************************************/
double kalman_prob_dens_func_dataPoint(double sample, double average, double std_dev)
{
	double prob = 0;
	double prob_0 = 0;
	double prob_1 = 0;
	double exp_0 = 0;
	double exp_1 = 0;
	double exp_2 = 0;

	exp_0 = pow((sample - average), 2);
	exp_0 = -1 * exp_0;
	exp_1 = pow(std_dev, 2);
	exp_1 = 2 * exp_1;
	exp_2 = exp_0 / exp_1;

	prob_0 = exp(exp_2);
	prob_1 = std_dev * sqrt(2 * PI_NUM);
	prob = prob_0 / prob_1;

	return prob;
}

/****************************************************************************
Function Name: gauss_dist
Programmer: Tessa Herzberger
Parameters taken: double array, double array
Parameter returned: Nothing
Program Description: Calls the calc_average and cald_std_deviation
functions to determine the gaussian
distribution of the RSSI sample array. Then calls the
prob_dens_func_dataPoint function to
determine the probability of receiving each
coresponding sample value. Since this is a
void function, it returns no parameters.
****************************************************************************/
double kalman_prediction(double probability_array[], double samples[])
{
	double prediction = 0.0;
	double current_max = probability_array[0];
	int max_location = 0;

	for (int i = 1; i < MAX_SAMPLES; i++)
	{
		if (current_max < probability_array[i])
		{
			current_max = probability_array[i];
			max_location = i;
		}
	}

//		cout << "The maximum probability value is " << probability_array[max_location] << endl;
//		cout << "The corresponding sample value is " << samples[max_location] << endl;
//		cout << endl;

	return samples[max_location];
}

void perform_kalman_filter(double samples[], double probability_array[], double predicted_vals[], double new_samples[])
{
	for (int i = 0; i < MAX_METERS; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ZERO_METERS_BASELINE[j];
		}
		if (i == 1)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ONE_METER_BASELINE[j];
		}
		if (i == 2)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWO_METERS_BASELINE[j];
		}
		if (i == 3)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THREE_METERS_BASELINE[j];
		}
		if (i == 4)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FOUR_METERS_BASELINE[j];
		}
		if (i == 5)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIVE_METERS_BASELINE[j];
		}
		if (i == 6)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SIX_METERS_BASELINE[j];
		}
		if (i == 7)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SEVEN_METERS_BASELINE[j];
		}
		if (i == 8)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = EIGHT_METERS_BASELINE[j];
		}
		if (i == 9)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = NINE_METERS_BASELINE[j];
		}
		if (i == 10)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TEN_METERS_BASELINE[j];
		}
		if (i == 15)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIFTEEN_METERS_BASELINE[j];
		}
		if (i == 20)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_METERS_BASELINE[j];
		}
		if (i == 25)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_FIVE_METERS_BASELINE[j];
		}
		if (i == 30)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THIRTY_METERS_BASELINE[j];
		}

	//	for (int i = 0; i < MAX_SAMPLES; i++)
	//		cout << "SAMPLES " << i << " :" << samples[i] << endl;

		//		FINISH THIS
		if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 ||
			i == 9 || i == 10 || i == 15 || i == 20 || i == 25 || i == 30)
		{
	//		cout << "******************* " << i << "METERS *******************" << endl;

//			for (int j = 0; j < MAX_SAMPLES; j++)
//				cout << "SAMPLES " << j << " :" << samples[j] << endl;

			predicted_vals[i] = kalman_filter(samples, predicted_vals, probability_array, new_samples);
//			removing_values_via_kalman_filter(samples, probability_array, new_samples);
//			predicted_vals[i] = kalman_prediction(probability_array, new_samples);

			//gauss_dist(probability_array, samples);
			//gaussian_filter(samples, probability_array, new_samples);
			//predicted_vals[i] = gaussian_prediction(probability_array, new_samples);
			cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
		}
		else
		{
			predicted_vals[i] = DEAD_BEEF_VAL;
			cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
			//cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
		}
	}

	return;
}

/****************************************************************************
		Function Name: removing_values_via_kalman_filter
		Programmer: Tessa Herzberger
		Parameters taken: double array, double array, double array
		Parameter returned: Nothing
		Program Description: Replaces all values with probabilities less
							 than the pre-defined minimum probability with
							 a "DEADBEEF" value of 9999.
****************************************************************************/
/*void removing_values_via_kalman_filter(double samples[], double prob_array[], double new_samples[])
{
	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (prob_array[i] > MIN_PROBABILITY)
			new_samples[i] = samples[i];
		else if (prob_array[i] <= MIN_PROBABILITY)
			new_samples[i] = DEAD_BEEF_VAL;
	}

	return;
}*/